%------------------------------------------
% Master Degree in Computer Science thesis
%------------------------------------------

\title{Incremental pretraining of multi-resolution memory networks}
\author{Diego Giorgini}

%------------------------------------------
% CONFIGURATIONS
%------------------------------------------

\documentclass[12pt, openright]{report}
\usepackage[utf8]{inputenc}  % Encoding
\usepackage{lmodern}
\usepackage[T1]{fontenc}
% \usepackage{fontspec}  % optional
\usepackage{pgfplots}
\pgfplotsset{compat=newest}
\usepgfplotslibrary{groupplots}
\usepgfplotslibrary{dateplot}
\usepackage{csquotes}  % typeset of quoted text for babel/polyGloss
\usepackage[english]{babel}
\renewcommand{\epsilon}{\varepsilon}
\renewcommand{\theta}{\vartheta}
\renewcommand{\phi}{\varphi}
\usepackage{graphicx}  % To include images \includegraphics
\usepackage{wrapfig} % boxes...
\usepackage{tcolorbox}
\usepackage{booktabs} % extended stuff for tables
\usepackage[justification=centering]{caption}
\usepackage{subcaption}
\usepackage{multirow} % /multicolumn in tables
\usepackage{float} % to specify the position of the table in the text
\usepackage{xcolor}
\definecolor{fancyblue}{rgb}{0,0.357,0.706}
\usepackage{titlesec}  % TO edit chapter/section... title look
\usepackage{fancyhdr}
\pagestyle{fancy}
\fancyhead{} % Clears the standard fancy style
\fancyhead[RO]{\nouppercase{\leftmark}}
\fancyheadoffset{0.00000001cm} % fix for headrule too short on the right 
\usepackage{emptypage}
\usepackage{tikz}
\usepackage[toc,page]{appendix}
\usepackage[framemethod=tikz]{mdframed}
\usepackage[acronym,toc,shortcuts,nomain]{glossaries}
\usetikzlibrary{positioning}
\usetikzlibrary{calc}
\usetikzlibrary{shapes, shapes.geometric, shapes.symbols, shapes.arrows, arrows.meta, shapes.multipart, shapes.callouts, shapes.misc}
\usetikzlibrary{patterns}

% "Chapter" # [Pipe] Title
\definecolor{gray75}{gray}{0.75}
\newcommand{\hsp}{\hspace{20px}}

% \titleformat{\chapter}[hang]
%     {\Huge\bfseries}
%     {\thechapter{} \hsp{} \textcolor{gray75}{|} \hsp}{0pt}{}

% \titlespacing*{\chapter}{0pt}{30pt}{50pt} %30 the last

\titleformat{\chapter}[display]
    {\LARGE\bfseries}
    {\chaptertitlename\ \thechapter}{20pt}{\Huge}

\usepackage{array}

\usepackage[backend=biber, bibencoding=ascii]{biblatex}
\usepackage{amssymb}
\usepackage{amsthm}  % Theorems and proofs
\usepackage{amsmath}
\usepackage{amsfonts}
% \usepackage{amssymb} % Additional symbols (e.g. \mathbb{N})
\usepackage{mathabx}
\usepackage{bm}  % Extended mathematics environment
\usepackage{siunitx}  % To write numbers and units
\sisetup{output-exponent-marker=\ensuremath{\mathrm{e}}}
\usepackage{enumitem}  % Personalize list environments (dotted point...)
\usepackage{url}  % To insert URLs
\usepackage[makeroom]{cancel}
\usepackage{xifthen}

% Settings dotted/numbered lists: topsep:spazio sopra e in fondo; itemsep: spazio tra linee; itemindent: spazio a sinistra
\setlist[itemize]{itemsep=0pt}
\setlist[enumerate]{itemsep=0pt}

\usepackage{microtype}                                  % Better letter spacing...
\usepackage[hidelinks, plainpages=false]{hyperref}  % hypertextual link
\usepackage{cleveref}                                   % Simplify make references. Use \cref
\usepackage[a4paper, heightrounded, top=3.25cm, bottom=3.25cm, left=3.75cm, right=3.25cm]{geometry}            % Page layout
\usepackage{setspace}                                   % line spacing
\onehalfspacing{}

\newenvironment{sidebox}[1][r]
  {\wrapfigure{#1}{0.5\textwidth}\tcolorbox}
  {\endtcolorbox\endwrapfigure}

\addbibresource{frontbackmatter/bibliography.bib}
\makeglossaries

\makeatletter

% Set title and author in the PDF
\hypersetup{%
    pdftitle={\@title},
    pdfauthor={\@author},
    pdfsubject={Master Thesis},
    pdfkeywords={machine learning, recurrent neural networks, linear memory networks, clockwork rnn, speech recognition},
    pdfproducer={pdfTeX-\the\pdftexversion\pdftexrevision}
}

\newenvironment{rcases}
  {\left.\begin{aligned}}
  {\end{aligned}\right\rbrace}
  
% Define front/main/backmatter as it is not defined in report document class
\newcommand\frontmatter {
    \if@openright{}
        \cleardoublepage{}
    \else
        \clearpage
    \fi
    \pagenumbering{roman}
}

\newcommand\mainmatter {
    \if@openright{}
        \cleardoublepage{}
    \else
        \clearpage
    \fi
    \pagenumbering{arabic}
}

\newcommand\backmatter {
    \if@openright{}
        \cleardoublepage{}
    \else
        \clearpage
    \fi
}

\def\blankpage{%
    \clearpage%
    \thispagestyle{empty}%
    \addtocounter{page}{-1}%
    \null%
    \clearpage}

\def\th@plain{% THEOREM STYLE
  \thm@notefont{}% same as heading font
  \itshape{}% body font
}
\def\th@definition{% Definition STYLE
  \thm@notefont{}% same as heading font
  \normalfont{}% body font
}

\renewenvironment{proof}[1][\proofname]{{\bfseries #1.}}{}  % Better proof definition
\renewcommand{\qed}{\hfill\square}  % Easier qed in proofs

\input{definitions/tikz}

\makeatother

\newtheorem{definition}{Definition} % definition numbers are dependent on theorem numbers

\newcounter{ListStartCount}% For items in enumerate that are multiline
\newenvironment{indenteditem}[1][]{%
    \setcounter{ListStartCount}{\theenumi}%
    \stepcounter{ListStartCount}%
    \begin{enumerate}[start=\theListStartCount,label={\arabic*.},#1]%
}{%
    \setcounter{enumi}{\theListStartCount}%
    \end{enumerate}%
}%

\def\code#1{texttt{#1}}  % Easier way to define code

%------------------------------------------
% DOCUMENT STRUCTURE
%------------------------------------------

\begin{document}

\input{frontbackmatter/glossary}

\frontmatter
    \input{frontbackmatter/titlepage}
    \input{frontbackmatter/abstract}
    \tableofcontents

\mainmatter
    % \input{snippets/test} % Keep only this line to generate the PDF for specific figures
    \input{chapters/introduction}
    \input{chapters/background}
    \input{chapters/clockwork-lmn}
    \input{chapters/experiments}
    \input{chapters/conclusions}
    
\backmatter
    % \input{frontbackmatter/acknowledgements}
    % \input{frontbackmatter/appendix}
    
    \printbibliography
    \addcontentsline{toc}{chapter}{Bibliography}
    \listoffigures
    \addcontentsline{toc}{chapter}{List of Figures}
    \listoftables
    \addcontentsline{toc}{chapter}{List of Tables}
    \printglossary[type=\acronymtype,title=List of Acronyms, style=long]

% \blankpage
    
\end{document}