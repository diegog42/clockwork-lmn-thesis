\chapter{Experiments}\label{ch:exp}

We compared our new model with a \gls{rnn}, a \gls{lstm}, a \gls{lmn} and a \gls{cwrnn}.

The tasks we chose are about audio signals because our network has modules that work at different time-scales, hence easily capturing information at different frequencies, which helps with long dependencies and global features of the sequence.

The first task, Sequence Generation, requires the model to predict the next element of a signal, given no input.
The second task, Spoken Word Classification, requires the model to distinguish spoken words. In this case, each sequence is preprocessed into \glspl{mfcc}~\cite{bib:mfcc}.

Every network has a single hidden layer. The activation function for the hidden layer is always \emph{tanh}. The total number of nodes is chosen to get approximately the same number of parameters to train.
The initial hidden state of the recurrent layer is set to 0. The initial values for all weights are drawn from a normal distribution with mean 0 and small standard deviation.
As optimization algorithm we used Adam~\cite{bib:adam} with L2 weight decay as regularization technique~\cite{bib:adam-wd}. For each model we executed a Random Search~\cite{bib:rs} in order to estimate the best hyperparameters.
The models are implemented with Pytorch 1.0~\cite{bib:pytorch} and tested on a CUDA enabled GPU \cite{bib:cuda}.


\section{Sequence Generation}

\input{snippets/exp-gen-tables}

The Sequence Generation task consists in taking a single sequence and training the model to predict, step by step, the next element of the same sequence. We used a sequence generated from a portion of a music file, sampling it at 44.1 KHz starting at a random position, turning it into 300 data points. The sequence elements were scaled to lie in the range \( [-1,1] \).
The task, having no input to memorize, only tests long-term dependencies. In \cite{bib:cwrnn} \gls{cwrnn} was shown to perform much better than \gls{rnn} and \gls{lstm}, so we wanted to see how \glspl{lmn} and \glspl{cwlmn} perform.

The model is provided with no input; at each timestep \( t \) the network is asked to produce as output the element of the sequence at time \( t+1 \). Since the output at each timestep is a single real number, the output layer has only one node, hence only a weight for each hidden neuron. This way the network is forced to encode information about the sequence into the hidden weights, without relying (too much) on the weights of the output layer.

We tested all models with approximately 4 different numbers of parameters: 100, 250, 500 and 1000 varying the number of hidden neurons (see Table~\ref{tab:gen-params}). The loss function we used the \gls{nmse} (the \gls{mse} divided by the range of possible values, in our case \( 2 \)).
In Table~\ref{tab:gen-hyperparams} you can see some of the final hyperparameters for each model.
With \gls{lmn} and \gls{cwlmn} we had better results with the outputs produced by the memory.
For \gls{cwlmn} we chose 9 modules with exponential clock rates \( \{1,2,4,8,16,32,64,128,256\} \).
More modules would be useless, as the sequence is 300 data points long.
The number of hidden units for the \gls{cwrnn} and of memory units for the \gls{cwlmn} is given by the number of modules times the number of units per module.
We obtained the best results by setting the forget gate of the \gls{lstm} to 5.

We show in Figure~\ref{fig:gen-plots} and report in Table~\ref{tab:gen-hyperparams} the results of our experiments. We show the reconstruction of one sequence with every model, selecting only the best configuration. From the figure we can see that:
\begin{itemize}
    \item The output of the \gls{rnn} is precisely reconstructed only for the first data points, then the mean of the output range is predicted for the rest of the sequence.
    That is the best example of the vanishing gradient problem;
    \item The output of the \gls{lstm} resembles a sliding average, with certain zones approximated more closely, other less so.
    Thanks to its gates it is able to memorize more information spread across the whole sequence, but it is not able to fit it completely.
    Moreover, we see that it has a limit to which it is able to learn, in the last part of the sequence it outputs a mean, like \gls{rnn} does.
    \item The output of the \gls{cwrnn} closely matches the whole sequence, but some areas are only approximated.
    This shows that a multi-scale architecture is very useful.
    The network closely matches the sequence, better than an \gls{lstm}, even at the end, but some parts are still guessed wrong.
    We can hypothesize that the fixed scales we chose were not perfect to represent information at those time steps.
    \item The output of the \gls{lmn} is a slightly worse sliding average with respect to \gls{lstm}, but the precision is of the same quality throughout the sequence.
    The memory is linear, thus free from one of the causes of vanishing gradient, but it is hard for a single module to internally represent information at different time-scales simultaneously;
    \item The output of the \gls{cwlmn} matches the sequence almost perfectly.
    We can guess that decoupling the functional components frees the memory (that has the same time-resolutions of \gls{cwrnn}) of solving the functional task, thus being able to store more information.
\end{itemize}

\begin{figure}[H]
    \centering
    \includegraphics[width=1.0\linewidth]{figures/gen_plots_v}
    \caption{The plots for the Sequence Generation task showing the original sequence we produced (dashed blue) overlapped with the reconstructed one (solid green).}\label{fig:gen-plots}
\end{figure}

\newpage

\section{Spoken Word Classification}

\paragraph{TIMIT dataset.}

The TIMIT~\cite{bib:timit} corpus is designed for training acoustic-phonetic models and automatic speech recognition systems.
It contains recordings from different speakers in major American dialects.
The dataset provides handmade splits and information about the phonemes that make words, without having to work with a vocabulary of words.
% This allows for more flexibility, but also different challenges.

TIMIT has been used for about three decades for research purposes: many models have been used, starting from~\cite{bib:timit-related-1-hmm} with \glspl{hmm}, \glspl{rnn}~\cite{bib:timit-related-2-rnn}, hybrid models \glspl{rnn}/\glspl{hmm}~\cite{bib:timit-related-2-rnn}, \glspl{mlp}~\cite{bib:timit-related-3-mlp}, specialized models such as \glspl{trap}~\cite{bib:timit-related-4-trap}, \glspl{dbn}~\cite{bib:timit-related-5-dbn}, kernel methods \cite{bib:timit-related-6-ker}, deep \gls{lstm}~\cite{bib:timit-related-7-deeplstm}, and deep convolutional networks~\cite{bib:timit-related-8-conv}, among others.

% We approach the dataset differently, instead of working with the whole dataset with phonemes, we construct a subtask, with the same experimental setup of \cite{bib:cwrnn}, take raw sequences, then construct features (\glspl{mfcc}~\cite{bib:mfcc}) out of the signal.

\paragraph{Task description and preprocessing.}

We took recordings of spoken sentences from different speakers from TIMIT.
We trained the model to recognize different words, regardless of the speaker.
To be specific, we followed the experimental setup defined in~\cite{bib:cwrnn}: we took 25 words pronounced by 7 different speakers, for a total of 175 examples. We can categorize them into 5 clusters, one for each common suffix. This suffix is useful to force the network to discriminate according to the first part of each word, as we want to test their ability to learn long-term dependency.
The clusters and words are the following:
\begin{itemize}
    \item \emph{Class 1}: making, walking, cooking, looking, working
    \item \emph{Class 2}: biblical, cyclical, technical, classical, critical
    \item \emph{Class 3}: tradition, addition, audition, recognition, competition
    \item \emph{Class 4}: musicians, discussions, regulations, accusations, conditions
    \item \emph{Class 5}: subway, leeway, freeway, highway, hallway
\end{itemize}
which are taken from the file codes reported in Table~\ref{tab:timit-files}.

The actual audio signal is in WAV format. Each file is a sentence, so we precisely trimmed it to the part related to the actual word using the metadata provided by the authors of the dataset.

At this point we had to prepare the data for speech recognition: we used a well-known technique from this field of research, by using the \glspl{mfcc} as features; they are designed to model the response of the human auditory system~\cite{bib:mfcc}. Some of the parameters used to extract the \glspl{mfcc} are the following: analysis window length: 25ms; step between successive windows: 1ms; preemphasis filter: 0.97; number of cepstrums to return: 13, but we replaced the zeroth cepstral coefficient, which is considered not useful, with the \emph{energy} (the log of total frame energy). As a result, we got 13 features.
Finally, we normalized each feature over the whole dataset in order to have mean 0 and variance 1.
We want to highlight that we work with \gls{mfcc} features related to the whole word signal, not with phonemes as other word-based approaches.

\input{snippets/exp-speech-codes}

\paragraph{Training procedure.}

For the training procedure, we wanted to have a comparable setting with the work of \cite{bib:cwrnn}, from which we took the multi-scale clockwork architecture.
We divided the dataset with the same split ratios, but not with the same assignments, as they were not indicated: in order to ensure balanced classes in both sets, we took 5 words for training and 2 words for testing from each class.
We found the best hyperparameters with a Random Search~\cite{bib:rs} as model selection.
We know using the test set this way leads to an overparameterization and an optimistic assessment of the performance of the model. 
Nonetheless what we are interested in is the ranking among the models, which is accurate, because we used the same model-selection for every model.
To alleviate this overestimation, after the parameters tuning phase in which we fixed the hyperparameters, we reshuffled the dataset before splitting into training and test set, made the final retraining and tested the models on this split.

Given \( 25 \) classes, the output layer of every network is a Softmax and every network was trained to minimize the Cross-Entropy Loss.
We used a batch size of \( 25 \), one sample per class to keep them balanced, shuffling the training set at the beginning of each epoch.

We limited the number of learned parameters to a maximum of 10000 (see Table~\ref{tab:speech-params} for the details about the number of hidden neurons). In Table~\ref{tab:speech-results} we report the best configuration in terms of accuracy over unseen examples and the corresponding hyperparameters.
Mean and standard deviation are computed over 25 runs with different weight initializations.
To remedy for the very small dataset, we added a Gaussian noise with a standard deviation of 0.6 to each training input sequence, but then computed the training error on the noise-free sequence to assess the model performance and produce the plots.
The correct number of epochs is fixed and decided with model selection.
For \gls{cwrnn} and \gls{cwlmn} we chose 7 modules with exponential clock rates \( \{1,2,4,8,16,32,64\} \), since the longer sequence has 97 data points.
The number of hidden units for the \gls{cwrnn} and of memory units for the \gls{cwlmn} is given by the number of modules times the number of units per module.
The actual number of \emph{learned} parameters for \gls{cwrnn} and \gls{cwlmn} is lower than the ones indicated because the lower blocks of the recurrent weight matrix remain \( 0 \) and represent disconnected modules.
All \glspl{lmn} produce the output from the memory component, as it performed better in our experiments.
We obtained the best results by setting the forget gate of the \gls{lstm} to 5.

About the pretraining, the unrolled model has an unrolling constant \( k \) of \( 15 \), learning rate \num{1e-03}, weight decay \num{1e-02} and was trained for \( 1000 \) epochs.
In the case of the incremental pretraining for \gls{cwlmn}, \( 7 \) unrolled networks have been trained, one for each module with the corresponding period.
Training time for slower modules was faster proportionally to the increase of the clock.
For instance, the fourth module has period \( 2^3=8 \), so we evaluate the network only once every \( 8 \) timesteps, and the training takes \( 1/8 \) of the time.
The best \gls{cwlmn} has been trained with \( 200 \) epochs in each i-step, for a total of \( 200 \times 7=1400 \) epochs.

\paragraph{Results.}

\input{snippets/exp-speech-results}

In Table~\ref{tab:speech-results} and Figure~\ref{fig:speech-results} we report the task performance of every model with the best hyperparameters tested on the final dataset split in terms of accuracy, mean and std.
In Figure~\ref{fig:speech-plots-loss} and Figure~\ref{fig:speech-plots-acc} we report the learning curves of the loss and of the accuracy for the final training.
We run the training \( 25 \) times. We show the loss over the training set in blue and over the validation set in orange; the mean is indicated by the solid line, and we also show the single measurements as dots at every timestep for every one of the \( 25 \) runs, giving an indication of the stability and the variance of the learning for every model.

\glspl{rnn} were completely unable to solve the task (accuracy \( 0.221 \)), while also requiring much more epochs (\( 10000 \)) than other models and showing much more variance (std \( 0.457 \)). They were clearly unable to go past the common suffix of the words and distinguish the words.

\glspl{lstm} are commonly used, in fact, they show good performance (\( 0.739 \)), a good variance and the minimum number epochs (\( 1400 \) in our case, as some of the other models).
% Gates enable the network to learn the correct dependencies.

Simple \glspl{lmn} did not perform very well on this task, with low performance (\( 0.505 \)), a high variance (\( 0.160 \)) and a higher number of epochs required.
While a higher learning rate could speed up the convergence, in practice for a linear recurrent model we found that high values of the learning rate led to instability in the learning process.

Multi-scale models such as \glspl{cwrnn} converge faster and are more stable, with less variance (\( 0.019 \)) and always good performance.
It is clear that separating the hidden states into modules allows the network to separate different kind of information and avoid internal interferences during updates.
The learning curve of \glspl{cwrnn} looks like it is slightly overfitting, but instead, the accuracy always grows.
Sadly we could not exactly replicate the results of the work we started from (\cite{bib:cwrnn}), as they reach with this model an accuracy of \(0.83\).

Instead, \glspl{cwlmn}, our new model, are stable during training as multi-scale ones, reaching the better performance (\( 0.799 \)) among un-pretrained models, with a low amount of epochs.
The advantage of the multi-resolution can be clearly seen by comparing the performance of the multi-resolution \gls{cwlmn} with the corresponding single resolution \gls{lmn}.
We also recall that they require much less learned parameters, as the recurrent weight matrix has \( 0 \) on blocks below the diagonal (non-existing connections between modules).

Now we discuss the pretraining we applied on \glspl{lmn} and \glspl{cwlmn}.
Both benefited from the pretraining, reaching a better performance due to the more principled initialization, compared to a random initialization. 
The former was now able to reach the performance of \glspl{cwrnn} and \gls{lstm} (\( 0.73 \)) with a third of the epochs required without pretraining, clearly showing the issue with regular \glspl{lmn} is not in its expressive power, but in a difficulty in training.
The learning curve clearly shows the effect of the pretraining steps.
The addition of a new module results in an initial spike in the loss, easily offset after a small number of epochs: we recall we used an incremental pretraining scheme in which every \( 200 \) epochs we added a module, initialized some weights with the solution of the linear autoencoder and some other randomly, thus requiring the network a few epochs to adjust to the new architecture.
After the first few epochs, the training is stable and the final performance is far higher than all other models (\( 0.87 \)), even better than the results we achieved with \gls{cwrnn} and the ones in the literature, with low std (\( 0.058 \)).

\input{snippets/exp-speech-plots}

\newpage

\paragraph{Benchmark tests.}

\input{snippets/exp-speech-timings}

To assess the time cost of all models, we report two kinds of measures.

In Figure~\ref{fig:speech-timings-100epochs} we report the time each model took to train \( 100 \) epochs (during the more costly phase, if more than one, as in the case of \glspl{cwlmn} and Unrolled) with respect to the fastest model.
The measurements of \glspl{rnn} and \gls{lstm} are many times better as they were already implemented with the \gls{cudnn} library \cite{bib:cudnn} by NVIDIA, while we only used Pytorch high-level abstractions to implement the other models.
We can observe that \glspl{lmn} and \glspl{cwlmn} are slower than \glspl{cwrnn}, as the forward pass consists of an additional phase: computing the hidden state of the functional component, then computing the memory.
\glspl{cwlmn} are slightly faster than \glspl{lmn} because, given the same learnable parameters, not all are executed at every timestep.

We also measured the whole training time (Figure~\ref{fig:speech-timings-whole}).
\glspl{lmn} are many times slower, as they take slightly more time to execute the same number of epochs, and they need a very high number of epochs to train them (\(6000\)).
By looking at the pretrained models, we can see that pret-\glspl{lmn} take much less time because they require a third of the epochs to complete their training.
Instead, pret-\glspl{cwlmn} take double the time of their not-pretrained counterparts, but also increased remarkably the performance.

Finally, we can observe that:
\begin{itemize}
    \item the number of weights in the recurrent matrix \(W^{mm} \in \mathbb{R}^{n \times n} \) (\( n = g N_m \)) of a \gls{cwlmn} is bigger than the number of actually trained weights, indeed we know from \cite{bib:cwrnn} that the number of trained parameters in \(W^{mm}\) of a \gls{cwrnn} (the same can be said for our model) is
    \begin{equation}
        \frac{{n^2}}{2} + \frac{n N_m}{2}.
    \end{equation}
    A regular \gls{rnn} would use the whole matrix, \( n^2 \) parameters.
    \item from the previous point they derive that \gls{cwrnn} has a theoretical speed-up of at least \( g/4 \) with respect to a \gls{rnn} (\cite{bib:cwrnn}).
    Even though we cannot use this result directly for the whole \gls{cwlmn}, the memory component is organized in modules exactly like a \gls{cwrnn}, hence there is a proportional speed-up with respect to a \gls{lmn}. 
    \item our model proved to require fewer parameters in order to achieve better results, also because of the separation into two simpler components.
    \item The incremental pretraining consisted in training \(7\) unrolled models and linear autoencoders. The total time to train all unrolled models tends to be double the time of training a single unrolled network on the whole sequence, because at every step we evaluate half the number of timesteps. Given \(U\) the maximum time of a single training (when the period is 1), then:
    \begin{equation}
        \sum_{i=0}^{g-1} \frac{U}{T_i} = U \sum_{i=0}^{g-1} \frac{1}{2^i} \leq 2U,
    \end{equation}
    which means that, regardless of how many modules we have, the training time for all unrolled networks does not increase beyond \( 2U \).
    This consideration does not include the model fine-tuning after pretraining, which still grows linearly with the number of epochs.
\end{itemize}


We can conclude that given an efficient implementation of \gls{cwlmn} and the unrolled network, this model could be competitive even when training time is important.


% Given \( U \) the time to train the first unrolled model and \( T_i = 2^i \) as the period of the current module (we assumed exponential module periods), then the total amount of time is given by the formula:
% \begin{equation}
%     \sum_{i=0}^{g-1} \frac{U}{T_i} = U \sum_{i=0}^{g-1} \frac{1}{2^i} = U (2 - 2^{1-g})
% \end{equation}