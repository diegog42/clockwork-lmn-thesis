# Available commands:
#
# $ make
# 	  	run latexmk continuously and stopping for errors
# $ make nonstop
#		run latexmk continuously but NOT stopping for errors
# $ make once
#		run latexmk once stopping for errors
# $ make debug
#		run directly pdflatex once

LATEX=pdflatex
LATEXOPT=--shell-escape
NONSTOP=--interaction=batchmode

LATEXMK=latexmk
LATEXMKOPT=-pdf -outdir=$(OUTDIR) -view=none
CONTINUOUS=-pvc

OUTDIR=out
MAIN=thesis
SOURCES=$(MAIN).tex Makefile frontbackmatter/* chapters/* snippets/*
FIGURES := $(shell find figures/* -type f)

all:    $(MAIN).pdf

.refresh:
		touch .refresh

$(MAIN).pdf: $(MAIN).tex .refresh $(SOURCES) $(FIGURES)
		$(LATEXMK) $(LATEXMKOPT) $(CONTINUOUS) -pdflatex="$(LATEX) $(LATEXOPT) $(NONSTOP) %O %S" $(MAIN)

force:
		rm $(MAIN).pdf
		$(LATEXMK) $(LATEXMKOPT) $(CONTINUOUS) -pdflatex="$(LATEX) $(LATEXOPT) %O %S" $(MAIN)

clean:
		$(LATEXMK) -C $(MAIN)
		rm -f $(OUTDIR)/$(MAIN).pdfsync
		rm -rf $(OUTDIR)/*~ $(OUTDIR)/*.tmp
		rm -f $(OUTDIR)/*.bbl $(OUTDIR)/*.blg $(OUTDIR)/*.aux $(OUTDIR)/*.end $(OUTDIR)/*.fls $(OUTDIR)/*.log $(OUTDIR)/*.out $(OUTDIR)/*.fdb_latexmk $(OUTDIR)/*.bcf $(OUTDIR)/*.run.xml $(OUTDIR)/*.thm $(OUTDIR)/*.toc

once:
		$(LATEXMK) $(LATEXMKOPT) -pdflatex="$(LATEX) $(LATEXOPT) %O %S" $(MAIN)

nonstop:
		$(LATEXMK) $(LATEXMKOPT) $(CONTINUOUS) -pdflatex="$(LATEX) $(LATEXOPT) $(LATEXOPT) $(NONSTOP) %O %S" $(MAIN)

debug:
		$(LATEX) $(LATEXOPT) $(MAIN)

.PHONY: all force clean once nonstop debug
