\chapter{Clockwork LMN}\label{ch:cwlmn}

\input{snippets/cwlmn-cwlmn}
\input{snippets/cwlmn-cwlmn-blocks.tex}

Our work consists in extending the memory component of \glspl{lmn} with multi-resolution modules as done in \glspl{cwrnn}. By doing this we expect to have a more flexible memory able to learn dependencies of different length (possibly higher-level information), while the linearity lessens the vanishing gradient effect. We named the new model \glspl{cwlmn}.

\section{Model architecture}

The basic architecture is shown in Figure~\ref{fig:cwlmn}. The functional component is the same as an \gls{lmn}, while the memory \( m \in \mathbb{R}^{g N_m} \) is organized in \( g \) modules of size \( N_m \).

\( h^t \in \mathbb{R}^{N_h} \) is the hidden state of the functional component. Given \(W^{xh}\) the weight matrix from the input to the hidden layer of the functional component, \(W^{m_i h}\) the weight matrix from memory module \(m_i\) to the hidden layer and \(\sigma\) the activation function \emph{tanh}, the equation to update \( h^t \) at timestep \( t \) is:
\begin{equation}\label{eq:cwlmn-ht}
    h^t = \sigma \left(W^{xh} x^t + \sum_{i=1}^g W^{m_{i}h} m_i^{t-1}\right).
\end{equation}
so at each update, \( h_t \) receives information from the whole memory.

The memory \( m \) is instead much different from an \gls{lmn}.
The memory is composed by \( g \) modules in which module \( i \) has clock rate \( T_i \in \{T_1,\ldots,T_g\} \). We update the state of module \( m_i \) with only information about slower or equal modules. The intuition is that faster modules are provided with the wider context, and slower modules are not distracted by low-level information. The simpler case is to use exponentially increasing clock rates \(\{ 1, 2, 4, 8, 16, 32, \ldots \}\): it seems a reasonable way of increasing the context size and it allows for a straightforward update policy. At any timestep \( t \) only modules with a clock such that
\begin{equation}\label{eq:update-rule}
    t \bmod T_i = 0    
\end{equation}
will be updated. We sort modules with non decreasing clock rates, such that for all \( i < j, T_i \leq T_j \), hence, at each timestep, we only update modules of contiguous indices always starting from module \( m_1 \).
An efficient implementation of the model can use a single block matrix for each set of weights: \( W^{hm} \in \mathbb{R}^{g N_m \times g N_h} \) holds all the weights from the state of the functional component to the memory:
\begin{equation}    
    W^{hm} = \left[ \begin{array}{ccc}
        W^{hm}_1 \\
        \vdots \\
        W^{hm}_g \\
    \end{array} \right].
\end{equation}  
\( W^{mm} \in \mathbb{R}^{g N_m \times g N_m} \) contains all the recurrent weights of the memory:
\begin{align}
    W^{mm} & = \left[ \begin{array}{ccc}
        W^{mm}_1 \\
        \vdots \\
        W^{mm}_g \\
    \end{array} \right]
\end{align}
\( W^{mo} \in \mathbb{R}^{N_o \times g N_m} \) has the weights of the output connected to the memory, where \( N_o \) is the size of the output layer:
\begin{equation}    
    W^{mo} = \left[ \begin{array}{ccc}
        W^{mo}_1 \\
        \vdots \\
        W^{mo}_g \\
    \end{array} \right].
\end{equation}  
Figure~\ref{fig:cwlmn-blocks} shows how the block structure is exploited to update the recurrent weights analogously to the \gls{cwrnn} case. \( b \) is the batch size and we excluded the bias from all figures and equations for simplicity.

% \( h^t \in \mathbb{R}^{N_h} \), \( W^{xh} \in \mathbb{R}^{N_h \times N_x} \) and
We can now define the equation used to update the memory:
\begin{equation}\label{eq:cwlmn-mt}
    m^t = \sigma \left(W^{hm} h^t + W^{mm} m^{t-1}\right),
\end{equation}
where, at each time step, we use block-rows of \(W^{mm}\) and \(W^{hm}\) that correspond to executed modules:
\begin{align}
    W^{mm}_i & = \begin{cases}
        W^{mm}_i \quad & \text{for} \ (t \bmod T_i) = 0 \\
        0 & \text{otherwise}
    \end{cases}\\
    W^{hm}_i & = \begin{cases}
        W^{hm}_i \quad & \text{for} \ (t \bmod T_i) = 0 \\
        0 & \text{otherwise}
    \end{cases}.
\end{align}

As for a regular \gls{lmn}, the output layer can be wired to the functional component, \( h^t \), with \( W^{ho} \in \mathbb{R}^{N_o \times N_h} \), or to the memory, \( m^t \):
\begin{align}
    y_h^t & = \sigma \left(W^{ho} h^t\right) \label{eq:cwlmn-yht}\\
    y_m^t & = \sigma \left(W^{mo} m^t\right) \label{eq:cwlmn-ymt}
\end{align}

The non-linearity used for computing the hidden states and the memory is \emph{tanh}. The network is trained through standard \gls{bptt}.

\section{Pretraining}

\input{snippets/cwlmn-pretr}
\input{snippets/cwlmn-pretr-blocks}

We extended the pretraining scheme based on the results on linear autoencoders for sequences in Section~\ref{sec:linae} and presented in Section~\ref{sec:lmn} in order to efficiently pretrain the \gls{cwlmn} modules in an incremental fashion.

The intuition is that, since what changes with respect to a regular \gls{lmn} is the architecture of the memory, we want to intervene in the compression phase of the explicit memory by encoding the past hidden states into a multi-resolution memory, instead of into a single module memory.
What we do is to construct the \gls{cwlmn} incrementally by building, pretraining with an approximate initialization algorithm, and then training the network, one module at a time.

% We briefly explain how it is possible to pretrain a linear autoencoder for sequences and obtain the matrices \( A \) and \( B \), then 
We briefly explain why we chose an incremental pretraining over a single step one, and then we describe the pretraining scheme in details.

% \paragraph{Summary of linear autoencoder closed-form solution.}

% We report a very brief summary of the equations used to find the closed form solution of the linear autoencoder. For more information see Section~\ref{sec:linae}.

% The linear autoencoder is a dynamical system described by the equation:
% \begin{equation}
%     y_t = Ax_t + By_{t-1} \label{eq:linae-y-2}
% \end{equation}
% where \( A \in \mathbb{R}^{N_m \times N_x}\), \( B \in \mathbb{R}^{N_m \times N_m}\). \( A \) contains the parameters related to the input, while \( B \) to the \emph{memory term}.
% We can define the state matrix \( Y \in \mathbb{R}^{l \times N_m }\), containing the encodings from all timesteps as:
% \begin{equation}
%     \underbrace{\left[ \begin{array}{cccc}
%         y_1^\top \\
%         y_2^\top \\
%         \vdots \\
%         y_l^\top
%     \end{array} \right]}_{Y} = \underbrace{\left[ \begin{array}{cccc}
%         x_1^\top & 0 & \cdots & 0 \\
%         x_2^\top & x_1^\top & \cdots & 0 \\
%         \vdots & \vdots & \ddots & \vdots \\
%         x_l^\top & x_{l-1}^\top & \cdots & x_1^\top
%     \end{array} \right]}_{\Xi} \underbrace{\left[ \begin{array}{cccc}
%         A^\top \\
%         A^\top B^\top \\
%         \vdots \\
%         A^\top B^{{l-1}^\top}
%     \end{array} \right]}_{\Omega}
% \end{equation}
% We factorize \( \Xi \) with its truncated SVD decomposition: \( \Xi = V \Sigma U^\top \). By solving the equation
% \begin{equation}
%     \Omega = \left[ \begin{array}{cccc}
%         A^\top \\
%         A^\top B^\top \\
%         \vdots \\
%         A^\top B^{{l-1}^\top}
%     \end{array} \right] = \left[ \begin{array}{cccc}
%         U_1^\top \\
%         U_2^\top \\
%         \vdots \\
%         U_l^\top
%     \end{array} \right] = U^\top
% \end{equation}
% with
% \begin{equation}
%     P \equiv \left[ \begin{array}{cc}
%         I_{N_x} \\
%         0_{N_x(l-1) \times N_x}^\top
%     \end{array} \right] \text{, \quad} R \equiv \left[ \begin{array}{cc}
%         0_{N_x \times N_x(l-1)} & 0_{N_x \times N_x}  \\
%         I_{N_x(l-1)} & 0_{N_x(l-1) \times N_x}
%     \end{array} \right]
% \end{equation}
% we can derive optimal \( A \) and \( B \) as
% \begin{equation}
%     A = U^\top P \text{, \quad} B = U^\top R U\text{.}
% \end{equation}

\paragraph{Incremental pretraining motivations.}

The natural extension for the pretraining of the \gls{lmn} would be to devise a mechanism to pretrain the weights of all modules together, but this would have an important drawback.
In the first pretraining step, we choose an unrolling factor \( k \) that represents how many hidden states we unroll and then we train the unrolled network.
Then, over the past states of this network, we build a linear autoencoder that compresses them into a single hidden state.
In the case of a multi-scale memory we could think of connecting the input layer and the encoding layer of the autoencoder in such a way as to replicate the behavior of the number of modules we want to have, but the slowest module should be able to get information from hidden states very far back in time, farther than \( k \) steps.
This means we would need to increase \( k \) accordingly, but this would lead to an unrolled network too expensive in terms of time and memory.

What we do instead is to build the network incrementally.
For clarity we call \emph{pretraining step (p-step)} the pretraining scheme of a single module, as described in Section~\ref{sec:lmn} and we call \emph{incremental step (i-step)} the whole procedure done to add a module, which contains many p-steps.

% Differently from the pretraining of a \gls{lmn}, in the incremental pretraining we use at each i-step the first two p-steps but slightly changing the third p-step, the final weight initialization.
% Apart from the incremental
In the next paragraph, we describe the whole procedure in details.

\paragraph{Incremental pretraining steps.}

Let us assume \( G \) to be the final number of modules, \( T_1 = 1 \) and \( T_2 = 2, \) to be the first two module periods.
Let us also define the names of the relevant weight matrices:
\begin{itemize}
    \item \(W^{hm}_j \in \mathbb{R}^{N_m \times N_h}\) is the weight matrix associated to the connection from the hidden state of the functional component to the memory component \(j\);
    \item \(W^{mm}_{ij} \in \mathbb{R}^{N_m \times N_m}\) is the (recurrent) weight matrix associated to the connection from the memory module \(i\) to memory module \(j\);
    \item \(W^{mh}_i \in \mathbb{R}^{N_h \times N_m}\) is the weight matrix associated to the connection from the memory module \(i\) to the hidden state of the functional component;
    \item \(W^{mo}_i \in \mathbb{R}^{N_o \times N_m}\) is the weight matrix associated to the connection from the memory module \(i\) to the output layer.
\end{itemize}
We refer throughout the following steps to Figure~\ref{fig:cwlmn-pretr} and Figure~\ref{fig:cwlmn-pretr-blocks}.
Biases are excluded from all dimensions and Figures.

\begin{itemize}
    \item \emph{i-step 1:} We start with an untrained \gls{cwlmn} with no modules, only the functional part, with equations:
    \begin{align}
        h^t & = \sigma \left(W^{xh} x^t \right)\\
        y_h^t & = \sigma \left(W^{ho} h^t\right),
    \end{align}
    where \(W^{xh} \in \mathbb{R}^{N_h \times N_x}\) and \(W^{ho} \in \mathbb{R}^{N_o \times N_h}\) are initialized with small random values.
    
    We want to add the first module:
    \begin{itemize}
        \item \emph{p-step 1:} We build an unrolled network with \( k \) previous hidden states other than \( h_t \) (Figure~\ref{fig:lmn-pretr-unfolded}).
        The unrolled network is defined by the following equations:
        \begin{align}
            \begin{rcases}
                h^t & = \sigma \left( W^{xh} x^t + \sum_{i=1}^{k}W^{hh}_i h^{t-i} \right)\\
                y^t & = \sigma \left( \sum_{i=0}^{k} W^o_i h^{t-i} \right)
            \end{rcases} \text{for} \ t \bmod T_i = 0, \label{eq:unrolled}
        \end{align}
        where \(W^{hh} \in \mathbb{R}^{N_h \times N_h}\) represents the relationship between the current hidden state and the state at time \(t-1\), while \(W^{o}_i \in \mathbb{R}^{N_o \times N_h}\) represents the relationship between the output and the hidden state at time \(t-1\).
        Being the first incremental step, we choose input values according to our first period, \( T_1 = 1 \), hence we use the whole sequence \(\{ x^0,x^1,\dots,x^l \}\), where \(l\) is the length of the sequence.
        To predict the first \( k \) output vectors we pad with \( 0 \) the missing inputs (inputs \( x^i \) with \( i < 0 \)).
        We train this network with backpropagation.

        \item \emph{p-step 2:} Based on equivalence results between recurrent and feedforward neural networks \cite{bib:equiv}, we can construct an explicit memory from the hidden states of the unrolled network (Figure~\ref{fig:lmn-pretr-explicit}).
        Intuitively, the hidden state \( h_t \) is the only state aware of the current \( x_t \), while \( \{h_{t-1},\dots,h_{t-k}\} \) consists in a memory of previous hidden states which are used to compute the next hidden state.

        We want to optimize this expensive explicit memory, so we build a linear autoencoder for sequences trained to compress all previous hidden states \( h_{t-1},\dots,h_{t-k} \) into an inner representation of fixed size \( N_m \).
        % Linear autoencoders basically compute PCA.
        Thanks to the results of \cite{bib:linae}, an optimal closed-form solution exists and can be used. 
        We end up with two parameter matrices, \( A \in \mathbb{R}^{N_m \times N_h} \) and \( B \in \mathbb{R}^{N_m \times N_m} \).
        
        \item \emph{p-step 3:} We use the obtained parameters to initialize the \gls{cwlmn}. The process is illustrated in Figure~\ref{fig:cwlmn-pretr-1}.
        We add the first module, resulting in a model with the equations of a \gls{cwlmn} as described earlier (\ref{eq:cwlmn-ht}, \ref{eq:cwlmn-mt}, \ref{eq:cwlmn-yht}, \ref{eq:cwlmn-ymt}), but only \( g = 1 \) modules, then we initialize the new matrices as follows:
        \begin{align}
            W^{hm}_{1}  & = A \\
            W^{mm}_{11} & = B \\
            W^{mh}_{1}  & = \mathcal{N}(0,\epsilon) \\
            W^{mo}_{1}  & = \mathcal{N}(0,\epsilon).
        \end{align}
        The last two matrices are initialized with a normal distribution with mean \( 0 \) and small variance because we want the network to learn those connection by itself and prevent the random initialization to interfere with pretraining results.
        
        At this point, we fine-tune the \gls{cwlmn} with backpropagation for some epochs before adding the second module.
    \end{itemize}

    \item \emph{i-steps 2,\(\dots\),G:} In summary, at every i-step we add a module to the \gls{cwlmn}, initialize the \emph{added} weights either with the result of the linear autoencoder trained on a new unrolled network (skipping some input elements according to the module period), or randomly, then fine-tune the \gls{cwlmn} for a fixed number of epochs.

    The newly added module should know the sequence at a different time-scale, so at each i-step we build a new unrolled model (equations \ref{eq:unrolled}), but every time we skip different elements of the input sequence, according to the period of the module.
    Given \( i \) the index of the current module, we visit only input elements \( x^t \) such that
    \begin{equation}
        t \bmod T_i = 0.
    \end{equation}
    For example in the second incremental step (see Figure~\ref{fig:cwlmn-pretr-2} and Figure~\ref{fig:cwlmn-pretr-blocks}) of our example, \( T_2 = 2 \), so we skip odd inputs at odd timesteps, using \(\{ x^0,x^2,\dots,x^l \}\).

    The linear autoencoder is trained as before on the previous hidden states of the unrolled network, \( h_{t-1},\dots,h_{t-k} \).

    We report the weight initialization for the general i-step \(j\) in which we add module \(j\):
    \begin{align}
        W^{hm}_{j}  & = A \\
        W^{mm}_{jk} & = \mathcal{N}(0,\epsilon) \quad \forall k \in \{1,\dots,j-1\} \\
        W^{mm}_{jj} & = B \\
        W^{mh}_{j} & = \mathcal{N}(0,\epsilon) \\
        W^{mo}_{j} & = \mathcal{N}(0,\epsilon).
    \end{align}
    
    Again, the newly added weights we do not get from the linear autoencoder are very small to minimize the initial influence of these connections.
    New biases included.
\end{itemize}
