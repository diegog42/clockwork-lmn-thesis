TODO / DOMANDE

<!-- costo computazionale -->
<!-- pretraining equazioni -->
<!-- togliere grassetto -->
<!-- passa grammarly -->

<!-- toglere pretr eq in più -->

# da riferire?
- https://github.com/jameslyons/python_speech_features
- https://github.com/bastibe/SoundFile

# fatto
- Introduction:
    - deve spiegare tutto dal punto di vista di uno che non ha letto la tesi ma è informatico. Devono capire problema e come lo affrontiamo e anche quello che otteniamo, non è un giallo
    - motivations:
        - vanishing
        - multi-resolution
        - temporal dependencies
        - hierarchical (abstract deep etc)
- Conclusioni:
    - deve esserci tutto, un po' come l'introduzione, ma dal punto di vista di chi ha letto tutto, con riflessioni informate

- future work:
    - invece di skippare fare pooling se vogliamo imparare cose gerarchiche
- Experiments:
    + risultati lmn, più epochs troppo instabile, così però ce ne vogliono 8000
    + dire che le lmn prendono l'output dalla memoria
    + parameters pretraining
    + batch size dirlo da qualche parte
    + commentare grafici generation con cose tipo: RNN è l'esempio del gradient vanishing; LSTM alla fine vediamo che non riesce a rappresentare gli ultimi dati; LMN essendo lineare non ha nonlinearity vanishing ma comunque si comporta come LSTM
    + loss cross entropy
    + Spoken Word: fare learning curve con media e std
    + SPoken Word: allenare di nuovo shuffleando e fissando però per ogni e la media.
    + Togliere discussion e metterle come paragrafo
    + histogram performance for comparison
    + TEMPO NUMERO EPOCHE
    + aggiungere perf lmn + pretlmn
+ Lista tabelle e figure
+ Background:
    + aggiungere "other related work" e discussion alla fine del capitolo con cosa è stato fatto, pro e contro.
+ Clockwork:
    + Equazioni pretraining anche qui perché deve essere self contained. Uno estrae il capitolo e lo implementa
    + spiegazione disegno pretraining
- References:
    + related work:
        + aggiungere gererchici
        + lavori su timit
    + paper LSTM
    + paper GRU
    + BPTT:
        - hinton
        - thesis di un certo verbos
    - pretraining
        + RBM pretraining
        + cos'è? definirlo? perché si fa?
    
-