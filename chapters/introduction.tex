\chapter{Introduction}\label{ch:intro}

\gls{ml}~\cite{bib:ml} is becoming one of the most important areas of Artificial Intelligence. It consists in using statistical models and inference to solve problems for which we do not have an equation nor `specific instructions'.
Very often someone manages to apply Machine Learning to difficult tasks previously tackled with other methods and obtain better performance with less prior knowledge about that particular task.
It has gained interest from the public and from companies that realized its importance and want to incorporate it in order to have an advantage over their competitors and some of its applications are starting to appear on our daily lives.



\gls{ml} models are able to learn complex dependencies in the data or learn hierarchical representations of information, as certain problems can only be solved when constructing higher and higher abstractions.
For example in vision we don't directly see a face as it is, but the part of our brain responsible for recognizing faces probably starts from colors, lighter and darker areas, shades, detect edges and depth, recognize part of objects, then puts things together in order to understand that the big object it has in front is a face, then compares features with others in memory in order to recognize as friendly or stranger.
Many problems need systems designed this way.
For instance, \glspl{cnn}, which work well in machine vision tasks \cite{bib:vision-1, bib:vision-2}, do not need prior knowledge of what are the best filters to use, as previously these were hand-made, but learn them by themselves, creating higher-level abstractions at each layer.

An important set of problems consists in requiring processing, identifying patterns in and making predictions with sequential or temporal data, such as data that has sequential or temporal dependencies among its components.
Data coming from physical sensors, sequences of images, speech are only a few of the many examples.
\glspl{rnn} is the paradigm of \glspl{ann} that deals with this kind of problems.
Since required pieces of information may be far from each other and since temporal data may as well have an inherently hierarchical nature, we need to research new models with this in mind.



% As hierarchical information in non-sequential data may be obtained by merging and pooling bits of data and then understanding what came out, in sequential data, abstract information may be something that spans from a point to another of the sequence, and only by having the two low-level information together we may understand the higher-level one.


This is why the issues in learning \glspl{rnn}, such as the vanishing gradient, or in general the problem in learning long-term dependencies, are crucial.

A well known standard line of research is the one dealing with \emph{gates}, such as \gls{lstm}~\cite{bib:lstm} and \glspl{gru}~\cite{bib:gru}, which allow certain information to flow forward and others to be forgotten, and the gradient to flow backward less degraded.

A more recent research is the one regarding hierarchical models, which is focused on the problem of finding hierarchies among temporal data.
The authors of \cite{bib:hierarchical-0-multi-scale-2} observed that information from higher hierarchies changes slower within the temporal sequence, hence some models have been devised with the aim to separate information at different resolutions in different parts of the architecture.
As \glspl{cnn} finds them with depth, \glspl{rnn} finds them with different speeds, as they are naturally deep networks with respect to time within the sequence.
The model we considered in this thesis is the \gls{cwrnn}~\cite{bib:cwrnn}, which employs a multi-scale architecture.



The second idea from which this thesis originated is model complexity.
Researchers often create more complicated models that solve a certain task better and better, and to make things more complex is sometimes necessary, but it is sometimes good to find ways to separate components in order to make them singularly simpler.
This way they may work even better (as we will show) and be conceptually more understandable.
Indeed, the work we started from is the one that introduces \glspl{lmn}~\cite{bib:lmn}, in which they conceptually separated the \emph{functional task} from the \emph{memory task}: the former consists in mapping the input to an output, the latter consists in retrieving the required information from the memory of past states.



Finally, training complex networks is difficult, so apart from several techniques that may help the optimization algorithm, there is also the approach of pretraining the network in such a way to start training it from somewhere in the parameter space closer to the solution, with the goal of making the learning faster, more stable and able to reach better solutions.
Pretraining was initially used to train \glspl{dbn}~\cite{bib:hierarchical-3-deep-intro-rbm,bib:hierarchical-3-deep-intro-rbm-2}, when it was difficult to train them directly.
After pretraining the layer pairwise, they fine-tuned the network with backpropagation.
Something similar can be done for sequential data, so we continue the work done in the context of \glspl{rnn} with \glspl{lmn} in \cite{bib:lmn}, in which they devised a pretraining scheme for the memory component of their network.

In brief, the contributions of this thesis can be summarized as follows. We took the conceptually simpler architecture of the \glspl{lmn} and we enhanced the memory with the multi-scale architecture from \glspl{cwrnn}, which makes it able to internally separate the representation of information at different resolutions, i.e. probably at hierarchically different heights, with the same amount of learned parameters, because making them separated decreases internal interferences.
This may seem counterintuitive when thinking about complexity, but we think that trying to model increasingly abstract information may require an architecture that mimics it.
We also built an incremental pretraining algorithm specific for our multi-resolution model; it is incremental in the sense that we build the memory incrementally, piece by piece: at each step we initialize the new memory module with pretrained weights, then we train it.
We have found that we were able to increase performance on a task of speech recognition, a subtask of TIMIT~\cite{bib:timit}, without increasing the number of learned parameters with respect to well-accepted models, such as \glspl{lstm}.

\section{Thesis outline}

In Chapter~\ref{ch:back} we introduce useful background material, such as our notation, an introduction on \glsdesc{ml} and \glsdescplural{nn}, a summary of the most important models for understanding the rest of this thesis and brief coverage of some other related works which share some of our goals.

In Chapter~\ref{ch:cwlmn} we define our new model, its architecture, how to build it efficiently by putting together all weights from different memory modules into a single matrix, and we describe our incremental pretraining algorithm.

In Chapter~\ref{ch:exp} we show experimental results on two tasks: a sequence generation one, and a speech recognition one.
We compare our models, with and without pretraining, with the basic \glspl{rnn}, the standard \glspl{lstm}, simpler multi-resolution \glspl{cwrnn}, and \glspl{lmn}.

Finally, in Chapter~\ref{ch:conc} we conclude this thesis with a discussion of the contributions of this work, as well as an analysis of the advantages and disadvantages of the proposed model. Finally, we discuss some future work that could be done to enhance it.