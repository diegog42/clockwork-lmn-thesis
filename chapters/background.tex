\chapter{Background}\label{ch:back}

This chapter covers some background material useful to understand the content of this thesis. We start with some notation in Section~\ref{sec:not}. Then we proceed with an introduction about \glsdesc{ml} in Section~\ref{sec:ml}. Then we introduce Neural Networks in Section~\ref{sec:nn}. Then we continue with a brief coverage of \glsdescplural{rnn} in Section~\ref{sec:rnn}, because all the models we show here belong to that class of Machine Learning models. Then we talk about \glsdescplural{cwrnn} in Section~\ref{sec:cwrnn}. To understand an interesting pretraining technique we cover Linear Autoencoders for Sequences in Section~\ref{sec:linae}, and \glsdescplural{lmn} in Section~\ref{sec:lmn}; we are especially interested in these, as our own model is an attempt at extending \glsdescplural{lmn} with a multi-resolution memory. Finally, in Section~\ref{sec:rel} we briefly discuss interesting related works.

\section{Notation}\label{sec:not}

This work is all about \glsdescplural{ann}, so we need to define the required notation about nodes, layers, and hidden states.

With \( x, h \) and \( y \) we refer respectively to the input vector, the hidden state vector and the output vector of the network. We use superscript numbers and letters to refer to a vector at a specific timestep, for instance \( h^0, h^1, h^t \), while we use subscript numbers to refer to a particular hidden state, when there are multiple ones, such as \( h_0, h_1, h_g \). The term \( m \) is used to refer to memory, a particular type of hidden state.

Uppercase letters such as \( A, B, V, \Xi \) represent matrices. In particular, \( W^{xh}, W^{ho} \) represent learnable weight matrices associated respectively to the connections between the input layer and the hidden layer in the former case, and from the hidden layer and the output layer in the latter.

We refer to a single weight associated with the connection from neuron \( x_i \) to neuron \( h_j \) with \( w^{xh}_{ij} \).

% When discussing about Clockwork models in Section~\ref{sec:cwrnn} and Chapter~\ref{ch:cwlmn} we use bold letters such as \( \mathbf{h} \) to differentiate them from their non bold counterpart; they indicate the concatenation of many similar vectors, for instance multiple hidden states \( h_i \); uppercase bold letters like \( \mathbf{W}^{hh} \) indicate a big block matrix that contains many smaller weight matrices.

% We use other lowercase letters to indicate vector and matrix dimensions or certain quantities, such as \( g, a, p, m, k, n \).
We use \( N_x, N_h, N_m, N_o \) to indicate dimensions related to neural network layers and lowercase letters like \( g, l, k, b \) to indicate other quantities.

We use \( I_{N_x}, 0_{N_x} \) to indicate special matrices of specific dimensions.

We use \( \sigma \) to indicate a non-linear activation function such as a \emph{tanh}.

With \( \mathcal{N}(0,\epsilon) \) we refer to the normal distribution with mean 0 and small variance.

\section{Machine Learning}\label{sec:ml}

With \glsdesc{ml} (ML)~\cite{bib:ml, bib:ml-2} we refer to the area of Artificial Intelligence that naturally intersects Computer Science and Statistics.
Computer Science builds computer systems that solve problems and studies their tractability, while Statistics, given some modeling assumptions, focuses on what can be inferred from data and how reliably.
As a consequence, \gls{ml} seeks to build computer systems that, instead of being manually programmed, program themselves; computer systems that learn how to infer information from data, changing their behavior all along the process, and studies the fundamental laws that govern learning processes.

Mitchell defines a learning task this way:
\begin{quotation}
    A computer program is said to learn from experience E with respect to some class of tasks T and performance measure P, if its performance at tasks in T, as measured by P, improves with experience E.
\end{quotation}

% Experience is given to the computer system as a set of data called Training Data, and the computer system has to change its inner workings in order to increase a particular performance measure with respect to a task, typically producing some kind of output.

\gls{ml} is particularly useful on hard problems in which a rigorous mathematical explanation (or simply an equation) is not known, hence we try to build a mathematical model that, relying on patterns and inference, is able to solve the problem –without having to identify an the explicit solution– for us.

\gls{ml} can be divided into different categories:
\begin{itemize}
    \item\emph{Supervised Learning:} the training data consists in input-output couples. The learning system is built to tune the parameters of the function it is optimizing by responding to error coming from the outputs it wrongly predicts. Input data with different outputs instructs the system on what makes data different, while input data with the same output instructs the system on what makes data similar.
    
    Under this category we can define two broad classes of tasks:
    \begin{itemize}
        \item \emph{Classification:} the output of the data is restricted to a limited set of discrete values, each element of this set is called \emph{class}. The mathematical model, given a training sample, should decide which class the sample belongs to (or output a probability distribution among them).
        \item \emph{Regression:} the output of the data is not restricted to a limited set, but it is allowed to take any numerical value inside a range. The learning model now has to produce a continuous value in the same range.
    \end{itemize}

    In certain cases it can be difficult in terms of economic or human effort to build a sufficiently large dataset, hence it is useful to study how a learning system may be able to derive information without outputs. This leads to the next type of learning task.
    \item \emph{Unsupervised Learning:} the training data does not supply expected outputs, so the learning model cannot be provided with a measurement of the error to guide learning.
    What the learning model can do instead is to find commonalities in the data, identify groups and clusters, and self-organize itself in such a way as to react, when given new data, based on the presence or absence of these commonalities.
    \item \emph{Reinforcement Learning:} this kind of learning is used when trying to model \emph{agents} that ought to take actions in an environment that provides stimulus, under a notion of cumulative reward. The objective is to maximize a \emph{reward}.
\end{itemize}

\section{Artificial Neural Networks}\label{sec:nn}

\input{snippets/back-nn}

\glspl{ann}~\cite{bib:nn} (which we will often call \glspl{nn} for simplicity) are systems inspired by the biology of the neurons inside our brain. An \gls{ann} is a large framework of mathematical models, all of them with the common idea of building a complex system through the connection of a lot of simple artificial neurons.

A not complex, yet important, \gls{nn} in the class of Feedforward Neural Networks, is called \gls{mlp}, and it is made of three layers (See Figure~\ref{fig:nn}). Let us assume we are talking about a single sample, this is made of a certain number of features (two in our example), one per node in the input layer and each feature is a real number.
In the \emph{forward pass} a certain input sample is given in input to the network and values flow from layer to layer up to the last one, in which an output is produced.
Each edge represents a connection and is associated to a weight, which strength is what actually changes through learning.

The core of the \gls{nn} is the structure of the node, or neuron. Let us assume we want to know the output of any one of the neurons of the hidden layer of Figure~\ref{fig:nn} and refer to the function it computes as \( h_j(x) \).
This function is the result of the application of an \emph{Activation Function} \( \sigma \) over the \( net \) of the neuron \( h_j \):
\begin{equation}
    h_j(x) = \sigma(net(x)) = \sigma\left(\sum\limits_{i=0}^{1}{w^{xh}_{ij}} + b\right)\label{eq:neuron}
\end{equation}
where \( w^{xh}_{ij} \) is the weight on the edge between \( x_i \) and \( h_j \) and \( \sigma \) is a special function that introduces non-linearity inside the computation of the neuron, which is fundamental, otherwise the network would only be able to learn linear functions.

There are many activation functions, some of them more suited for a particular task. In order to be able to train the network (more on this later) we need to be able to compute derivatives, hence we need differentiable functions.
Three of the most famous ones are the \emph{logistic} function (or \emph{sigmoid}), the \emph{hyperbolic tangent} and the Softmax. The first produces values in the continuous range \( (0,1) \), while the second in \( (-1,1)\), and the last reshapes an output vector with one value for each class to become a probability distribution: every value is in range \( (0,1) \) and they must sum to one.
We report the equation of the first two and their derivative:
\begin{align}
    s(x)                  & = \frac{1}{1 + e^{-x}} & s'(x) & = s(x)(1-s(x)) \\
    \text{\emph{tanh}}(x) & = \frac{e^x - e^{-x}}{e^x + e^{-x}} & \text{\emph{tanh}}'(x) & = 1-{\text{\emph{tanh}}(x)}^2.
    % \text{\emph{softmax}}(x) & = \frac{e^{x_c}}{\sum_{c=1}^{C}e^{x_c}} for i = 1,\dots,C & 
\end{align}
% where \( C \) is the number of classes.

\paragraph{Learning.}

\glspl{nn} are typically trained with Backpropagation~\cite{bib:bp}. The idea of this algorithm is to compute the forward pass, compute a loss function (or error function) \( \mathcal{L} \) representing a measure of the inaccuracy of the prediction, then backpropagate the error backward layer by layer to every weight of the network.

What we actually compute is the partial derivative of the loss function with respect to the network parameters. Learning in this context is actually like setting up a non convex optimization problem in which the weights represent a point inside the space of all solutions the network is able to reach and the gradient of the error represents the direction in which the point should move in order to increase or decrease the function (the loss).
A standard way to approach the problem is to use an optimization algorithm: the most common is Stochastic Gradient Descent.
What it does it to simply move in the direction pointing to a local minimum.
We know this may not be the global optimum of the function, that's why a lot of techniques have been proposed to help the search through the space of all solutions.

% We do not explain the details of backpropagation nor how the weights are updated, as we just need to have the intuition about them; we will be more rigorous on things more useful for the purposes of this thesis.

\section{Recurrent Neural Networks}\label{sec:rnn}

\input{snippets/back-rnn}
\input{snippets/back-rnn-deep}

An \gls{rnn} is a class of Neural Networks specialized to work with sequences as its input, such as temporal sequences, as it is able to encode information (dependencies among particular elements) about the sequence in its hidden state.
The difference with non-recurrent Neural Networks is that they have cycles, allowing their internal state to be updated step by step, giving them the capacity to have a dynamic memory of the history of inputs, therefore handling sequences of inputs with temporal dependencies.

Figure~\ref{fig:rnn} depicts a generic \gls{rnn} with its unfolding. The simplest form of \gls{rnn} has a single layer of hidden units \( h \) connected to itself. The hidden state \( h \) is actually a sequence of hidden states \( h^t \), one for every timestep \( t \).
At each timestep a single element \( x^t \) of the input sequence \( \{x^1,x^2,\dots,x^t,\dots,x^l\} \) is provided to the hidden units together with the previous hidden state, \( h^{t-1} \), in order to update the hidden state and produce an output \( y^t \). Of all the outputs produced, the task decides which are useful: for instance in classification tasks such as predicting the class of the input sequence only the last one is kept, while in tasks such as generating or predicting the next word of a sentence, an output at each timestep is required.

The state and the output of a \emph{vanilla} \gls{rnn} is updated with the following formulas:
\begin{align}
    h^t & = \sigma \left( W^x x^t + W^h h^{t-1} \right) \\
    y^t & = W^o h^{t-1}.
\end{align}

\paragraph{Training.}

\glspl{rnn} are trained with an extension of backpropagation called \gls{bptt}~\cite{bib:bptt1,bib:bptt2,bib:bptt3}. We will also discuss about a more practical variant, the Truncated \gls{bptt}~\cite{bib:tbptt}.

The idea of \gls{bptt} is to compute the contribution to the gradient on a network unfolded over \( k \) timesteps. For now let us assume \( k = l\). Hence, we have \( k \) input-output couples \( \left<(x^0, y^0), (x^1, y^1), \ldots, (x^k, y^k)\right> \). We completely unfold the network, forward-propagate, compute the predicted output and the error, then compute the gradient backwards, and finally update the weights.

Given long sequences, this kind of network often suffers from vanishing and exploding gradients \cite{bib:vanexp}, due to the repeated multiplications when backpropagating, leading to the inability to learn long dependencies and to the convergence to bad local minima. In Figure~\ref{fig:rnn-deep} we can see how an unfolded \gls{rnn} resembles a very deep feedforward network with weight sharing between the hidden layers.
The reason behind this issue is that when backpropagating through a lot of timesteps, in order to compute the partial derivative of the loss \( \mathcal{L} \) with respect to a distant hidden state, \( {\partial \mathcal{L}} / {\partial h_k} \), the recurrent weight matrix and the derivative of the activation function of the recurrent unit appears to the power of \( l-k \).
By applying the chain rule it can be shown that
\begin{equation}
    \left\lVert \frac{\partial \mathcal{L}}{\partial h_k} \right\rVert \leq \left\lVert W^{l-k} \right\rVert 
\end{equation}
where \( W^{l-k} \) is the recurrent weight matrix to the power of \( l-k \).
It follows that the influence is determined by its biggest eigenvalue \( \lambda \): if \( \lambda > 1 \), then the gradients will tend to explode, if \( \lambda < 1 \) then the gradient tend to vanish.
As for the activation function, it contributes to the vanishing gradient, as the derivative of the function has a maximum value of \( 1 / 4 \) for a sigmoid and \( 1 \) for a \emph{tanh}, and both have a minimum value of \( 0 \), thus reducing the gradient more and more.

One of the most simple way to approach exploding gradients and make the computation more efficient for very long sequences (or streaming endless sequences) is to just unfold the network up to a \( k \) much smaller than the length of the sequence (this is called Truncated \gls{bptt}), compute the error, update the weights, then proceed to unfold the next \( k \) timesteps. This is not always a solution, as it precludes the network from learning long-term dependencies.

Another issue is that at every step all weights are updated, hence long-term dependencies often gets forgotten in favor of recent information, often useless for generalizing over new sequences.

Models has been devised with either or both of this issues in mind, for instance all models with \emph{gates}, such as \gls{lstm}~\cite{bib:lstm} and \glspl{gru}~\cite{bib:gru}, or even \glspl{cwrnn} allow information to flow in the forward pass from very old nodes directly to the ones in the current timestep, while allowing the gradient to flow backward through the same connection without being diminished by many gradient computation. \glspl{lmn}~\cite{bib:lmn} employs a linear memory which is internally updated without activation functions nor gates.

We discuss more in depth about \gls{cwrnn}, \glspl{lmn} and how the problem is faced in the literature in the next sections.

\section{Clockwork RNN}\label{sec:cwrnn}

\input{snippets/back-cwrnn}
\input{snippets/back-cwrnn-blocks}
% \input{snippets/back-cwrnn2}

Instead of using gates to avoid low-level information overwriting long dependecies, \glspl{cwrnn}~\cite{bib:cwrnn} tries to separate the hidden states into different components, each with its own time resolution, so that faster module learn short-term dependencies, while slower ones learn long-term ones.

\glspl{cwrnn} organizes the hidden nodes into \( g \) modules that are updated at different clock rates or periods. The network architecture is shown in Figure~\ref{fig:cwrnn}.
Each module is connected to all non-decreasing clock rate modules. We order the modules with increasing clock rates \( \{T_1,\ldots,T_g\} \) and assign them increasing indices \( \{1,\ldots,g\} \), then each module \( i \) gives its contribution to itself and all the faster modules to its left.
This architecture is effective because slower modules, updating every once in a while, focus on the broader context, while providing information about long-term dependencies to the faster modules which focus on short-term ones.

Before introducing blocks, let us define or recall some quantities: \( N_x \) is the input size; \( g \) is the number of modules, \( N_h \) is the hidden size; \( N_o \) is the output size.

In order to make the computation more parallelizable (especially on GPU), instead of considering the hidden states as separate nodes, we can exploit properties of block matrices to aggregate their weights into a single matrix, and their states into a single one as well.
In particular, instead of having \( g \) set of weights from input to hidden layer \( \{W^{x,h_1},\ldots,W^{x,h_g}\} \), \( g \) set of recurrent weights \( \{W^{h,h_1},\ldots,W^{h,h_g}\} \) and \( g \) set of output weights \( \{W^{h_1,o},\ldots,W^{h_g,o}\} \), we only need three block matrices: \( W^{xh} \in \mathbb{R}^{g N_h \times g N_x} \) which holds all the weights from the input layer to the hidden layer, \( W^{hh} \in \mathbb{R}^{g N_h \times g N_h} \) which holds all the recurrent weights, and \( W^{ho} \in \mathbb{R}^{N_o \times g N_h} \) for the output layer.
Moreover, we call the matrix containing the state of all modules at a specific timestep \( h^t \). In Figure~\ref{fig:cwrnn-blocks} we show the matrices graphically and how to update the hidden states. These are the equations used to compute the recurrent step and the output:
\begin{align}
    h^t & = \sigma \left(W^{hh} h^{t-1} + W^{xh} x^t \right) \label{eq:ht}\\
    y^t & = \sigma \left(W^{o} h^t\right)
\end{align}

Equation~\eqref{eq:ht} assumes we update the state of all modules, but using block matrices has the advantage that, if we do not need to update a module, we can slice out the relative block-row from the weight matrices, and make the computation faster as a consequence.

To be more precise, the weight matrix \( W^{hh} \) is made of \( g \) block rows, each one made of other \( g \) blocks, one for every other module. Every one of these matrices contains weights between couples of modules.
Since modules do not contribute to the update of slower modules, as we explained earlier, all blocks below the diagonal are \( 0 \). The other weight matrix \( W^{xh} \) is similar, but each block-row contains weights to connect a module to the input layer (\( N_x \) weights).

\section{Linear Autoencoder for Sequences}\label{sec:linae}

Nonlinear autoencoder networks have become famous for their ability to build an internal representation with interesting properties of a set of inputs.
In this thesis we focus on the results of \cite{bib:linae}, in which it is shown how linear autoencoders perform PCA on some input data and how to apply them to different data structures.
We are, of course, interested in sequences, as performing PCA on a set of sequences is equivalent to find a linear system that compresses the data into the most efficient state vector.
We will use such compression to mitigate the vanishing gradient issue of training long recurrent networks.
Autoencoders are typically trained through backpropagation, or as \glspl{rbm}, but when dealing with linear ones, closed form solutions exist.

Let us see a summary of how to get the closed formula to train linear autoencoders.

Given a temporal sequence as input \( \{x_1,x_2,\dots,x_t,\dots,x_l\} \), the dynamical system is defined by the following equations:
\begin{align}
    y_t & = Ax_t + By_{t-1} \label{eq:linae-y} \\
    \left[ \begin{array}{ccc}
        x_t\\
        y_{t-1}\\
    \end{array} \right] & = Cy_t
\end{align}
where \( A \in \mathbb{R}^{N_m \times N_x}\), \( B \in \mathbb{R}^{N_m \times N_m}\) and \( C \in \mathbb{R}^{(N_x+N_m) \times N_m}\) are the model parameters\footnote{In order to make the notation more uniform, we are already using the dimensions we are going to use in the section where these concepts will be applied.}. \( A \) contains the parameters related to the input, while \( B \) to the \emph{memory term}. We define the state matrix \( Y \in \mathbb{R}^{l \times N_m }\) as the matrix that encodes the input sequence from all timesteps, then we decompose each \( y_{t} \) with its equation (\ref{eq:linae-y}) up to \( y_0 = 0 \), and factorize the input vectors \( x_t \) from the parameter matrices \( N_x \) and \( B\) into:
\begin{equation}
    \underbrace{\left[ \begin{array}{cccc}
        y_1^\top \\
        y_2^\top \\
        \vdots \\
        y_l^\top
    \end{array} \right]}_{Y} = \underbrace{\left[ \begin{array}{cccc}
        x_1^\top & 0 & \cdots & 0 \\
        x_2^\top & x_1^\top & \cdots & 0 \\
        \vdots & \vdots & \ddots & \vdots \\
        x_l^\top & x_{l-1}^\top & \cdots & x_1^\top
    \end{array} \right]}_{\Xi} \underbrace{\left[ \begin{array}{cccc}
        A^\top \\
        A^\top B^\top \\
        \vdots \\
        A^\top B^{{l-1}^\top}
    \end{array} \right]}_{\Omega}.
\end{equation}
We factorize \( \Xi \) with its truncated SVD decomposition: \( \Xi = V \Sigma U^\top \). The proof starts by imposing \( \Omega = U \), exploiting the structure of \( \Xi \) and by noticing that, since \( U^\top \) is orthogonal, then \( \Omega U^\top = I \), and ends by concluding that we are able to derive optimal \( A \) and \( B \) (by only keeping as much rows as \( rank(\Xi) \)).
Given the decomposition of \( U \) into \( l \) submatrices, the problem reduces to find matrices \(A\) and \(B\) such that:
\begin{equation}
    \Omega = \left[ \begin{array}{cccc}
        A^\top \\
        A^\top B^\top \\
        \vdots \\
        A^\top B^{{l-1}^\top}
    \end{array} \right] = \left[ \begin{array}{cccc}
        U_1 \\
        U_2 \\
        \vdots \\
        U_l
    \end{array} \right] = U.
\end{equation}

Since we are using an SVD decomposition, \( \Sigma \) contains the singular values and each column of \( U_t^\top \) is a left-singular vector and corresponds to a principal direction of the data, i.e. PCA (apart from technicalities).

For more details and an in-depth proof, see \cite{bib:linae}. It is enough for us to say that the former equation is solved by defining the matrices
\begin{equation}
    P \equiv \left[ \begin{array}{cc}
        I_{N_x} \\
        0_{N_x(l-1) \times N_x}^\top
    \end{array} \right] \text{, \quad} R \equiv \left[ \begin{array}{cc}
        0_{N_x \times N_x(l-1)} & 0_{N_x \times N_x}  \\
        I_{N_x(l-1)} & 0_{N_x(l-1) \times N_x}
    \end{array} \right]
\end{equation}
and by computing:
\begin{equation}
    A = U^\top P \text{, \quad} B = U^\top R U,
\end{equation}
where \( I_x \) is the identity matrix of size \( N_x \) and \( 0_{N_x \times N_x} \) is the zero matrix of size \( N_x \times N_x \).

We will see later how this is exploited for pretraining the memory component of the \glsdesc{lmn} and how it can be easily extended to a memory with a different structure, such as a multi-resolution one.

\section{Linear Memory Networks (LMN)}\label{sec:lmn}

\input{snippets/back-lmn.tex}
\input{snippets/back-pretraining}
\input{snippets/back-lmn-linae}

\glspl{lmn} arise from the intuition that sequence processing problems may be decomposed into two separate tasks: a \emph{functional} one, in which the network has to map the input sequence into a certain output and encodes non-linearities, and a \emph{memory} task, in which information needs to be stored and retrieved in order to be able to produce the output that the functional part has selected.
An architecture like this may be less complex, because by decoupling the two parts, they can be simpler: in \glspl{lmn} the functional part is a simple fully connected layer, while the memory is a linear autoencoder for sequences.
The hidden states are computed with the following equations:
\begin{align}
    h^t & = \sigma \left(W^{xh} x^t + W^{mh} m^{t-1}\right)\\
    m^t & = W^{hm} h^t + W^{mm} m^{t-1}.
\end{align}
The output activation is standard, apart from the way it can be connected to the rest of the network, either to the functional part, or to the memory, resulting in one of the two equations:
\begin{align}
    o_h^t & = \sigma \left(W^{ho} h^t\right)\\
    o_m^t & = \sigma \left(W^{mo} m^t\right).
\end{align}

\paragraph{Pretraining.}

Pretraining techniques are useful when dealing with hard-to-learn models.
We initialize the model with parameters such that they represent a point in the space of all solutions which is nearer to the optimal one, thus hopefully avoiding the training process to lead to bad local minima.

An interesting pretraining technique was first used to train \glspl{dbn}~\cite{bib:hierarchical-3-deep-intro-rbm,bib:hierarchical-3-deep-intro-rbm-2}.
What the authors did is to pretrain all layers, two adjacent ones at a time, like \glspl{rbm} with the contrastive divergence algorithm \cite{bib:hierarchical-3-deep-intro-rbm-3}, then fine-tune the network (especially the output layer) with backpropagation.

What we repropose here is a pretraining scheme for encoding into the weights of the \gls{lmn} (thus, into the memory component) the necessary information to reconstruct the hidden state sequence, then train the network starting from there \cite{bib:lmn}.

The memory is a linear autoencoder, one of the properties of the linearity is that we can derive an explicit solution to find the optimal encoding (as discussed in section~\ref{sec:linae}) and use it to initialize the memory.

The general idea is to first build an unfolded version of the network, then decouple function and memory, and then bring it into an efficient form.

In the first step we build a recurrent network with an unfolding up to \( k \) timesteps in the past.
The output at time \( t \) depends on the current hidden state and \( k \) previous ones (Figure~\ref{fig:lmn-pretr-unfolded}).
Each hidden state is computed with information coming from the current input and from \( k \) previous time steps.
We train this network through standard backpropagation.

In the second step, we take the hidden states computed this way and look at it at a different perspective: we want to take the previous hidden states with respect to time \( t \) and construct a memory from them. The idea is that only \( h_t \) has access to the current input \( x_t \), thus it is probably the one solving the input-output transformation at time \( t \), while the other hidden states should just be a memory of past states. We call \( \{ h_{t-1},\dots,h_{t-k} \} \) the explicit memory for \( h_t \). This is based on equivalence results between recurrent and feedforward neural networks \cite{bib:equiv}. 

The explicit representation is inefficient, so in the third step we want to compress the memory. We train a linear autoencoder to encode a sequence of hidden states into an efficient representation. In Figure~\ref{fig:linae} we show the general architecture of an autoencoder for our problem. It is possible to solve the task with an optimal closed form solution; as seen in section~\ref{sec:linae} a linear autoencoder basically performs PCA.

Solving the linear autoencoder allows us to obtain the input matrix \( A \) and the recurrent matrix \( B \).
We can use them to initialize the \gls{lmn}, then fine tune it with backpropagation.
We also need to recall \( U = \left[ A^\top, A^{\top}B^\top, \dots, A^{\top}B^{{k-1}^\top} \right] \).
Given all this, we initialize the parameters of the \gls{lmn} in the following way:
\begin{align}
    W^{hm} & = A \\
    W^{mm} & = B \\
    W^{mh} & = \left[W_1^{hh}, \dots, W_k^{hh}\right]\ U \\
    W^{hm} & = \left[W_1^{o}, \dots, W_k^{o}\right]\ U
\end{align}
then train it with backpropagation.

\section{Other related work}\label{sec:rel}

In this section we briefly present another research line that tries to tackle hierarchical information processing, some promising models specifically aimed at reducing the vanishing gradient problem.
%  and some of the research on speech recognition.
% that has been done on the famous dataset we are going to use, TIMIT\cite{bib:timit}.

\paragraph{Hierarchical models}

Deep learning~\cite{bib:hierarchical-3-deep-ai} became famous for being able to learn increasingly more abstract representations of information with its highest layers, for instance in problems in which information is organized spatially, deep convolutional neural networks were shown to be able to classify images on ImageNet much better than any other previous model \cite{bib:hierarchical-4-conv}.

Temporal data may as well have an inherently hierarchical nature~\cite{bib:hierarchical-0-multi-scale-2}, thus the need to devise new recurrent models able to capture this kind of structure inside temporal data.
It has been observed that higher-level abstractions change slower within temporal sequences with respect to lower-level ones.
Since that, many models and approaches emerged: multi-scale \glspl{rnn}, such as \gls{skiprnn}~\cite{bib:hierarchical-2-skip-rnn}, or \gls{hmrnn}~\cite{bib:hierarchical-0-multi-scale-1,bib:hierarchical-0-multi-scale-2} up to \glspl{cwrnn}~\cite{bib:cwrnn}, of which we talked about in Section~\ref{sec:cwrnn} and we extended in Chapter~\ref{ch:cwlmn}.

In \gls{lstm}~\cite{bib:lstm}, each hidden unit has update and forget gates, thus can learn multi-scale information, but the timescales are not organized hierarchically and information are still diluted over hundreds of time steps due to leaky integration.

Under the line of \emph{conditional updates}, one approach, aimed at reducing the side effects of very long sequences on the training, is \gls{skiprnn}~\cite{bib:hierarchical-2-skip-rnn}.
When the sequence is very long, or efficiency is important, a suboptimal solution is to sample the data less frequently, or simply decide to skip certain sequence values.
\gls{skiprnn} introduces the concept of a learnable parameters, used as a probability to update the hidden states.
With this, the network may learn to skip a certain number of useless steps.
One of the cons of this approach is that the network has to be trained with a reinforcement learning manner, and determining the reward may be non trivial.

\glspl{cwrnn}~\cite{bib:cwrnn}, of which we discussed in a dedicated section (\ref{sec:cwrnn}), follows this line of research, as it tries to learn different scale dependencies by separating the hidden states into modules that run at different speeds.

Another approach with explicit boundaries is \gls{hrnn}~\cite{bib:hierarchical-2-hrnn}.
This model is used for neural machine translation; word boundaries are obtained through word tokenization.

Some work has been done trying to process the sequence in its entirety, instead of a traditional online manner (one step at a time), such as in \cite{bib:hierarchical-2-whole-1,bib:hierarchical-2-whole-2}.

The authors of \cite{bib:hierarchical-2-sample-1,bib:hierarchical-2-sample-2} tried to do merging and pooling of low-level representation with the aim to obtain shorter sequences representing higher-level abstractions.

The last approach we cite is \gls{hmrnn}~\cite{bib:hierarchical-1-models} that tries to discover the latent hierarchical information inside temporal sequences by making the model learn \emph{boundaries} which identify places where different scale information begin or end, and adding some operations, in a manner similar to gated networks, to intuitively let the model decide to update or not the hidden state based on those boundaries.

We chose to work with \glspl{cwrnn}: some of the advantages are that slower modules are less affected by vanishing gradient, we can update slower modules less often, and we can decide how many neurons to dedicate to each module (i.e. each representation at a certain time resolution).
One of the disadvantages is that the different time-scales are an hyperparameter, while in many contexts, if data is non-stationary in temporal sequences, it may be needed to have the network learn it by itself.

\paragraph{Orthogonality.}

A line of research that is directly focused on one of the two causes of vanishing (and exploding) gradients \cite{bib:vanexp} is the one that revolves around orthogonal matrices.
The idea introduced in \cite{bib:orthogonal-1-intro}, is that, since the problem is caused by repeated multiplications of a recurrent matrix that have spectral radius smaller or bigger than 1, we should try to find a way to enforce the recurrent weight matrix to be orthogonal –unitary in their case, as they work in the complex field.
They devise a composition of matrices with certain properties in order to yield a weight matrix with such properties.

Enforcing this constraint perfectly is not necessarily the best thing to do. Other researchers working on this idea proposed another kind of solution: instead of using a special decomposition, they introduce a \emph{soft bound}~\cite{bib:orthogonal-2-soft}: a special regularization term that penalizes the loss when the spectral radius is far from 1, hence enforcing the constraint only asymptotically.
Given matrix \( W \), \( W^\top W = I \) if and only if \( W \) is orthogonal. Hence the regularization term is given by how far \( W \) is from being orthogonal:
\begin{equation}
    \lambda \left\lVert W^\top W - I \right\rVert .
\end{equation}

In \cite{bib:orthogonal-3-kru} it is shown hot to make the network less parametrized and make this product more efficient by decomposing the recurrent weight matrix as the Kronecker product of a few smaller matrices, and enforcing the constraint on the smaller matrices.